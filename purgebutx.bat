@echo off
rem -------------------------------------------------------------------------------------------
rem This script deletes files older than the X newest files.
rem It doesnt actually compare dates, just sorts the files by date and then skips over X newest.
rem For help/syntax run: purgebutx.bat /h
rem -------------------------------------------------------------------------------------------
setlocal enabledelayedexpansion
set keep=20
set purgedir="C:\temp\log"
set filefilter="*.log"
set bugout=0
call:checkforswitches %*
if %bugout%==1 goto:eof

call:main %purgedir% %filefilter%

goto:eof

rem -------------------------------------------------------------------------------------------
:main  - so I'm an old C programmer, sue me...
set ftsw=1
set dir=%~1
set filter=%~2

echo.
echo purgebutx.bat - Delete files in the specified path (yes you can specify wildcard filename)
echo                 but keep the newest X number of files. For help/syntax run: purgebutx.bat /h
echo.
rem echo dir=%dir% filter=%filter%
rem Fixup trailing backslash
if not [%dir:~-1,1%]==[\] set dir=%dir%\

set purgepath="%dir%%filter%"

rem Sanity check
if exist "%dir%" goto dirok
echo.
echo Error: Cannot find directory: %dir%
echo.
goto:eof
:dirok
echo Deleting all except %keep% %filefilter% files in %dir%
echo.

for /f "tokens=* delims=*" %%v in ('dir /tw /o-d /b %purgepath%') do (
	if !ftsw! GTR !keep! del "%dir%%%v"
	call set /a ftsw+=1
	)
goto:eof
rem -------------------------------------------------------------------------------------------
:checkforswitches
if [%1]==[/h] goto showhelp
if [%1]==[/H] goto showhelp
if [%1]==[/d] goto getpurgedir
if [%1]==[/D] goto getpurgedir
if [%1]==[/f] goto getfilefilter
if [%1]==[/F] goto getfilefilter
if [%1]==[/x] goto getkeep
if [%1]==[/X] goto getkeep
goto:eof

:getkeep
call set /a keep="%2%"*1
shift
shift
if keep GTR 0 goto checkforswitches
echo.
echo Error: Invalid number of files to keep.
echo.
set bugout=1
goto:eof

:getfilefilter
set filefilter=%2%
shift
shift
goto:eof

:getpurgedir
set purgedir=%2
shift
shift
if exist %purgedir% goto checkforswitches
echo.
echo Error: Cannot find path: %purgedir%
echo.
set bugout=1
goto:eof

:showhelp
rem Strip surrounding quotes
for /f "useback tokens=*" %%a in ('%purgedir%') do set purgedir=%%~a
for /f "useback tokens=*" %%a in ('%filefilter%') do set filefilter=%%~a
echo  Syntax:
echo     purgebutx.bat [/x X] [/d directory] [/f filter]
echo                   [/h]       show this text
echo.
echo  Defaults:                  (if you want to change the defaults go right ahead!)
echo     X         = %keep%
echo     directory = %purgedir%
echo     filter    = %filefilter%
echo.
echo  Examples:
echo     Show the help
echo        purgebutx.bat /h
echo     Keep the 3 newest and delete the older files ending with .TMP in the C:\TEMP directory
echo        purgebutx.bat /x 3 /d C:\temp /f *.tmp
echo.