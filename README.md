A Windows Batch script.
You will need a reasonably new Windows to run this script, I'm guessing Windows
2000 or later.
The script is meant to be run as a scheduled task to clean out, for example,
log files from a directory.
You specify which directory, a wildcarded filename to match and the number of
files (the latest) that should remain, any more older files than that are deleted.